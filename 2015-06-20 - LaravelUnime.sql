-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.21 - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para laravel_unime
CREATE DATABASE IF NOT EXISTS `laravel_unime` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `laravel_unime`;


-- Copiando estrutura para tabela laravel_unime.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cpf_cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cpf_cnpj` (`cpf_cnpj`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela laravel_unime.cliente: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` (`id`, `nome`, `cpf_cnpj`, `email`, `telefone`, `created_at`, `updated_at`) VALUES
	(1, 'Felix Costa da Silva da Jhan', '067.096.885-42', 'fx3costa@gmail.com', '7192777051', '2015-06-11 20:52:52', '2015-06-13 18:03:47'),
	(6, 'Joaoa', '323.434.343-43', 'fx3costa@gmail.com', '7192777051', '2015-06-11 23:41:11', '2015-06-13 10:52:11'),
	(8, 'Werdum', '111.111.111-11', 'werdum@teste.com', '(23) 4443-55334', '2015-06-19 00:52:58', '2015-06-19 00:52:58');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;


-- Copiando estrutura para tabela laravel_unime.conta
CREATE TABLE IF NOT EXISTS `conta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contratoId` int(10) unsigned NOT NULL,
  `leituraId` int(10) unsigned NOT NULL,
  `numero` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dataEmissao` datetime NOT NULL,
  `dataVencimento` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valorTotal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `conta_contratoid_foreign` (`contratoId`),
  KEY `FK_conta_leitura` (`leituraId`),
  CONSTRAINT `FK_conta_leitura` FOREIGN KEY (`leituraId`) REFERENCES `leitura` (`id`),
  CONSTRAINT `conta_contratoid_foreign` FOREIGN KEY (`contratoId`) REFERENCES `contrato` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela laravel_unime.conta: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `conta` DISABLE KEYS */;
INSERT INTO `conta` (`id`, `contratoId`, `leituraId`, `numero`, `dataEmissao`, `dataVencimento`, `valorTotal`, `created_at`, `updated_at`) VALUES
	(2, 1, 20, '', '0000-00-00 00:00:00', '2015-07-20 00:00:00', '280', '2015-06-20 12:28:18', '2015-06-20 12:28:18'),
	(3, 1, 21, '', '0000-00-00 00:00:00', '2015-07-20 00:00:00', '300', '2015-06-20 12:30:11', '2015-06-20 12:30:11');
/*!40000 ALTER TABLE `conta` ENABLE KEYS */;


-- Copiando estrutura para tabela laravel_unime.contrato
CREATE TABLE IF NOT EXISTS `contrato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clienteId` int(10) unsigned DEFAULT NULL,
  `dataCriacao` datetime DEFAULT CURRENT_TIMESTAMP,
  `numero` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precoKW` double DEFAULT NULL,
  `enderecoId` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `contrato_clienteid_foreign` (`clienteId`),
  KEY `FK_contrato_endereco` (`enderecoId`),
  CONSTRAINT `FK_contrato_endereco` FOREIGN KEY (`enderecoId`) REFERENCES `endereco` (`id`),
  CONSTRAINT `contrato_clienteid_foreign` FOREIGN KEY (`clienteId`) REFERENCES `cliente` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela laravel_unime.contrato: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
INSERT INTO `contrato` (`id`, `clienteId`, `dataCriacao`, `numero`, `precoKW`, `enderecoId`, `created_at`, `updated_at`) VALUES
	(1, 8, NULL, '6545465', 2, NULL, '2015-06-20 10:33:39', '2015-06-20 10:33:39'),
	(2, 8, '2015-06-20 10:37:55', '141654', 3, NULL, '2015-06-20 10:37:55', '2015-06-20 10:37:55'),
	(3, 8, '2015-06-20 10:38:42', '6545646545', 4, NULL, '2015-06-20 10:38:42', '2015-06-20 10:38:42');
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;


-- Copiando estrutura para tabela laravel_unime.endereco
CREATE TABLE IF NOT EXISTS `endereco` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logradouro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela laravel_unime.endereco: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `endereco` DISABLE KEYS */;
INSERT INTO `endereco` (`id`, `logradouro`, `cidade`, `bairro`, `cep`, `estado`, `created_at`, `updated_at`) VALUES
	(1, 'saddsadsa', 'Lauro de Freitas', 'dsadasdas', '42700000', 'BA - Bahia', '2015-06-11 20:52:52', '2015-06-18 23:29:31'),
	(6, '', 'Lauro de Freitas', 'Itingaaaaaaaa', '42700000', 'BA', '2015-06-11 23:41:11', '2015-06-14 22:30:25'),
	(8, 'Rua do UFC', 'Chicago', 'NA', '88888-822', 'WA', '2015-06-19 00:52:58', '2015-06-19 00:52:58');
/*!40000 ALTER TABLE `endereco` ENABLE KEYS */;


-- Copiando estrutura para tabela laravel_unime.leitura
CREATE TABLE IF NOT EXISTS `leitura` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tecnicoId` int(10) unsigned NOT NULL,
  `relogioId` int(10) unsigned NOT NULL,
  `quantidade` double unsigned NOT NULL,
  `dataLeitura` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `leitura_tecnicoid_foreign` (`tecnicoId`),
  KEY `leitura_relogioid_foreign` (`relogioId`),
  CONSTRAINT `leitura_relogioid_foreign` FOREIGN KEY (`relogioId`) REFERENCES `relogio` (`id`),
  CONSTRAINT `leitura_tecnicoid_foreign` FOREIGN KEY (`tecnicoId`) REFERENCES `tecnico` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela laravel_unime.leitura: ~18 rows (aproximadamente)
/*!40000 ALTER TABLE `leitura` DISABLE KEYS */;
INSERT INTO `leitura` (`id`, `tecnicoId`, `relogioId`, `quantidade`, `dataLeitura`, `created_at`, `updated_at`) VALUES
	(20, 1, 1, 140, '0000-00-00 00:00:00', '2015-06-20 12:28:17', '2015-06-20 12:28:17'),
	(21, 1, 1, 150, '2015-06-05 00:00:00', '2015-06-20 12:30:11', '2015-06-20 12:30:11');
/*!40000 ALTER TABLE `leitura` ENABLE KEYS */;


-- Copiando estrutura para tabela laravel_unime.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela laravel_unime.migrations: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2014_10_12_000000_create_users_table', 1),
	('2014_10_12_100000_create_password_resets_table', 1),
	('2015_06_04_223804_clientetable', 2),
	('2015_06_04_223805_clienteenderecotable', 2),
	('2015_06_04_223820_tecnicotable', 2),
	('2015_06_04_223849_contratotable', 2),
	('2015_06_04_223904_contatable', 2),
	('2015_06_04_223922_relogiotable', 2),
	('2015_06_04_223935_leituratable', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Copiando estrutura para tabela laravel_unime.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela laravel_unime.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;


-- Copiando estrutura para tabela laravel_unime.relogio
CREATE TABLE IF NOT EXISTS `relogio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contratoId` int(10) unsigned NOT NULL,
  `identificacao` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `relogio_contratoid_foreign` (`contratoId`),
  CONSTRAINT `relogio_contratoid_foreign` FOREIGN KEY (`contratoId`) REFERENCES `contrato` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela laravel_unime.relogio: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `relogio` DISABLE KEYS */;
INSERT INTO `relogio` (`id`, `contratoId`, `identificacao`, `created_at`, `updated_at`) VALUES
	(1, 1, '123132', '2015-06-20 11:22:19', '2015-06-20 11:22:19');
/*!40000 ALTER TABLE `relogio` ENABLE KEYS */;


-- Copiando estrutura para tabela laravel_unime.tecnico
CREATE TABLE IF NOT EXISTS `tecnico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `regiaoAtuacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_tecnico_users` (`userId`),
  CONSTRAINT `FK_tecnico_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela laravel_unime.tecnico: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `tecnico` DISABLE KEYS */;
INSERT INTO `tecnico` (`id`, `userId`, `nome`, `regiaoAtuacao`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Joao', '546545', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tecnico` ENABLE KEYS */;


-- Copiando estrutura para tabela laravel_unime.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria` int(10) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela laravel_unime.users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `categoria`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Felix', 0, 'fx3costa@gmail.com', '$2y$10$I7PFg0NHn7R2nE6FZt6GNOHx/7fdAi1.GkTaMPFVi9fUGDLjnAAZC', 'q4rB1N8WxHAtzlqbENIaaX6Il01g1lvOZqgxe059hOTlkkWQksumBz8vWV7l', '2015-06-05 22:24:14', '2015-06-20 09:32:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
