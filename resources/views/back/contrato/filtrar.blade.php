<?php 
	$setBusca  = '';
	isset($busca) ? $setBusca = $busca : $setBusca = '';
?>

{!! Form::open(array('url' => 'contrato/buscar', 'class' => 'form-inline')) !!}
  <div class="form-group">
    <label class="sr-only" for="exampleInputEmail3">Cliente</label>
    {!! Form::text('cliente', null, array('id' => 'cliente', 'class' => 'form-control', 'style' => '', 'placeholder' => 'Nome, email')) !!}
  </div>
  <button type="submit" class="btn btn-default">Filtrar</button>
  <button type="button" class="btn btn-default" id="limpar">Limpar</button>
{!! Form::close() !!}

<script language="javascript" type="text/javascript">
$(document).ready(function(){
	
	$('#cliente').val("<?php echo $setBusca; ?>");
	
	$('#limpar').click(function(e){
		$('#cliente').val("");
		window.location.href = '/contrato/index';
	});
});
</script>