@extends('layouts.layout') 
@section('content')

	<h2>Edição de Contrato</h2>
	<hr/>
	<?php //dd($contrato->numero); ?>
	{!! Form::model($contrato, ['route' => ['contratoUp', $contrato->id], 'method' => 'put']) !!}
		
		<div class="form-group">
			{!! Form::label('numero', 'Número: ') !!}
			{!! Form::text('numero', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		</div>
		
		{!! Form::label('cliente', 'Cliente: ') !!}
		<div class="input-group">
			{!! Form::text('cliente', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
			<a class="btn btn-primary" type="" id="buscarCliente">Buscar</a>
		</div><br/>
		
<!-- 		campo escondido que conterá o id do cliente para salvar o contrato -->
		{!! Form::hidden('clienteId', null, array('class' => 'form-control', 'style' => 'width:300px', 'id' => 'clienteId')) !!}
		
		<div class="form-group">
			{!! Form::label('logradouro', 'Logradouro: ') !!}
			{!! Form::text('logradouro', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('numero', 'Número: ') !!}
			{!! Form::text('numero', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('cidade', 'Cidade: ') !!}
			{!! Form::text('cidade', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('bairro', 'Bairro: ') !!}
			{!! Form::text('bairro', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('estado', 'Estado: ') !!}
			{!! Form::text('estado', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('complemento', 'Complemento: ') !!}
			{!! Form::text('complemento', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('cep', 'CEP: ') !!}
			{!! Form::text('cep', null, array('class' => 'form-control cep', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::submit('Enviar', ['class' => 'btn btn-default']) !!}
			<button class="btn btn-danger">Cancelar</button>
		</div>
	{!! Form::close() !!}

<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		$('.cpf_cnpj').mask('000.000.000-00', {reverse: false	});
		$('.telefone').mask('(00) 0000-00000');
		$('.cep').mask('00000-000');
		});
</script>
@endsection