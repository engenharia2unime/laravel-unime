@extends('layouts.layout') 
@section('content')

	<h2>Cadastro de Contrato</h2>
	<hr/>
	
	{!! Form::open(array('url' => 'contrato')) !!}
		
		<div class="form-group">
			{!! Form::label('numero', 'Número: ') !!}
			{!! Form::text('numero', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		</div>
		
		{!! Form::label('cliente', 'Cliente: ') !!}
		<div class="input-group">
			{!! Form::text('cliente', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
			<a class="btn btn-primary" type="" id="buscarCliente">Buscar</a>
		</div><br/>
		
<!-- 		campo escondido que conterá o id do cliente para salvar o contrato -->
		{!! Form::hidden('clienteId', null, array('class' => 'form-control', 'style' => 'width:300px', 'id' => 'clienteId')) !!}
		
		<div class="form-group">
			{!! Form::label('logradouro', 'Logradouro: ') !!}
			{!! Form::text('logradouro', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('numero', 'Número: ') !!}
			{!! Form::text('numero', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('cidade', 'Cidade: ') !!}
			{!! Form::text('cidade', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('bairro', 'Bairro: ') !!}
			{!! Form::text('bairro', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('estado', 'Estado: ') !!}
			{!! Form::text('estado', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('complemento', 'Complemento: ') !!}
			{!! Form::text('complemento', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('cep', 'CEP: ') !!}
			{!! Form::text('cep', null, array('class' => 'form-control cep', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::submit('Enviar', ['class' => 'btn btn-default']) !!}
			<a class="btn btn-danger" href="/contrato/index">Cancelar</a>
		</div>
		
	{!! Form::close() !!}

	
<script language="javascript" type="text/javascript">

	$(document).ready(function(){
		$('.cpf_cnpj').mask('000.000.000-00', {reverse: false});
		$('.telefone').mask('(00) 0000-00000');
		$('.cep').mask('00000-000');
		$('#cliente').mask('000.000.000-00', {reverse: false});
		
		$('.select2').select2({
			placeholder: "Selecione um cliente",
			allowClear: true,
			});

		
		$("#buscarCliente").click(function(){
			var cliente = $('#cliente').val();
			cliente ? buscarCliente(cliente) : alert('Digite um CPF válido!');
		});

		
		// Função ajax para buscar cliente	
		function buscarCliente(cliente){
			$.ajax({
				  url: "/cliente/buscar-cliente",
				  dataType: 'json',
				  data: { cliente: cliente}
				})
			  .done(function(data) {
				var obj = JSON.parse(data.cliente);
				
				if(obj != ''){						// verificar se é o melhor método de condição para caso haja cliente
					alert( "Cliente Encontrado!"); // melhorar para algo mais carismático
				    $("#cliente").val(obj[0].nome);
				    $("#clienteId").val(obj[0].id);
				}else{
					alert("Cliente não encontrado! Verifique o CPF digitado!");
					}
			  });
		}

	});
</script>
@endsection