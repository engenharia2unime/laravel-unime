@extends('layouts.layout') 
@section('content')
 			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Contratos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="col-lg-12">
 					@include('back.contrato.filtrar')
 				</div>
 			</div><br/>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        	<a class="btn btn-primary" href="{{ action('ContratoController@create') }}">Adicionar Contrato</a>
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Número Contrato</th>
                                            <th>Cliente</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($contratos as $contrato)
                                        <tr class="odd gradeX">
                                            <td>{{ $contrato->numero }}</td>
                                            <td>{{ $contrato->cliente['nome'] }}</td>
                                           <td style="width:300px">
                                           		<a class="btn btn-success" href="{{ action('RelogioController@index', [$contrato->id]) }}">Relógios</a>
												<a class="btn btn-primary" href="{{ action('ContratoController@edit', [$contrato->id]) }}">Editar</a>
												<a class="btn btn-danger" href="">Deletar</a>
											</td>
                                        </tr>
                                    	@endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@endsection