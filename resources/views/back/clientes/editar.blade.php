@extends('layouts.layout') 
@section('content')

	<h2>Edição de Cliente</h2>
	<hr/>
	
	{!! Form::model($cliente, ['route' => ['clienteUp', $cliente->id], 'method' => 'put']) !!}
		
		<div class="form-group">
			{!! Form::label('nome', 'Nome: ') !!}
			{!! Form::text('nome', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		</div>
		
		<div class="form-group">
			{!! Form::label('cpf_cnpj', 'CPF: ') !!}
			{!! Form::text('cpf_cnpj', null, array('class' => 'form-control cpf_cnpj', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('email', 'Email: ') !!}
			{!! Form::email('email', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::label('telefone', 'Telefone: ') !!}
			{!! Form::text('telefone', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		</div>
		
		<div class="form-group">
			{!! Form::submit('Enviar', ['class' => 'btn btn-default']) !!}
			<button class="btn btn-danger">Cancelar</button>
		</div>
	{!! Form::close() !!}

<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		$('.cpf_cnpj').mask('000.000.000-00', {reverse: false	});
		$('.telefone').mask('(00) 0000-00000');
		$('.cep').mask('00000-000');
		});
</script>
@endsection