@extends('layouts.layout') 
@section('content')
 			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Clientes</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
            	<div class="col-lg-12">
 					@include('back.clientes.filtrar')
 				</div>
 			</div><br/>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        	<a class="btn btn-primary" href="{{ action('ClienteController@create') }}">Adicionar Cliente</a>
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Email</th>
                                            <th>Telefone</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($clientes))
	                                    @foreach($clientes as $cliente)
	                                        <tr class="odd gradeX">
	                                            <td>{{ $cliente->nome }}</td>
	                                            <td>{{ $cliente->email }}</td>
	                                            <td>{{ $cliente->telefone }}</td>
	                                           <td style="width:200px">
													<a class="btn btn-primary" href="{{ action('ClienteController@edit', [$cliente->id]) }}">Editar</a>
													<a class="btn btn-danger" href="{{ action('ClienteController@destroy', [$cliente->id]) }}">Deletar</a>
												</td>
	                                        </tr>
	                                    @endforeach
	                               	@else
	                               	 <tr class="odd gradeX">
                                       	<td colspan="4">Nenhum registro encontrado!</td>
	                                  </tr>
	                               	@endif 
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@endsection