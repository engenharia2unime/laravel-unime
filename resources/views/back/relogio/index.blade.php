@extends('layouts.layout') 
@section('content')
 			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Relógios</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        	<a class="btn btn-primary" href="{{ action('RelogioController@create', $contrato) }}">Adicionar Relógio</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Identificação</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($relogios as $relogio)	
                                        <tr class="odd gradeX">
                                            <td>{{ $relogio->identificacao }}</td>
                                            <td style="width:300px">
                                            		<a class="btn btn-success" href="{{ action('LeituraController@index', [$relogio->id]) }}">Leituras</a>
													<a class="btn btn-primary" href="">Editar</a>
													<a class="btn btn-danger" href="">Deletar</a>
											</td>
                                        </tr>
                                   @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@endsection