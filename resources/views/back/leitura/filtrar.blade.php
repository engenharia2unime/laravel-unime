<?php 
	$setBusca  = '';
	isset($busca) ? $setBusca = $busca : $setBusca = '';
?>

{!! Form::open(array('url' => 'leitura/buscar', 'class' => 'form-inline')) !!}
  <div class="form-group">
    <label class="sr-only" for="exampleInputEmail3">Relógio</label>
    {!! Form::text('relogio', null, array('id' => 'relogio', 'class' => 'form-control', 'style' => '', 'placeholder' => 'Identificação do Relógio')) !!}
  </div>
  <button type="submit" class="btn btn-default">Filtrar</button>
  <button type="button" class="btn btn-default" id="limpar">Limpar</button>
{!! Form::close() !!}

<script language="javascript" type="text/javascript">
$(document).ready(function(){
	
	$('#relogio').val("<?php echo $setBusca; ?>");
	
	$('#limpar').click(function(e){
		$('#relogio').val("");
		window.location.href = '/leitura/index';
	});
});
</script>