@extends('layouts.layout') 
@section('content')

	<h2>Cadastro de Leituras</h2>
	<hr/>
	
	{!! Form::open(array('url' => 'leitura')) !!}
		
		<div class="form-group">
			{!! Form::label('quantidade', 'Quantidade KW: ') !!}
			{!! Form::text('quantidade', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		</div>
		
		<div class="form-group">
			{!! Form::label('dataLeitura', 'Data da Leitura: ') !!}
			{!! Form::date('dataLeitura', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		</div>
		
		{!! Form::hidden('relogioId', $relogio, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		
		<div class="form-group">
			{!! Form::submit('Enviar', ['class' => 'btn btn-default']) !!}
			<a class="btn btn-danger" href="/leitura/index/{{ $relogio }}">Cancelar</a>
		</div>
	{!! Form::close() !!}

	
<script language="javascript" type="text/javascript">

	$(document).ready(function(){
		$('.cpf_cnpj').mask('000.000.000-00', {reverse: false});
		$('.telefone').mask('(00) 0000-00000');
		$('.cep').mask('00000-000');
		$('#cliente').mask('000.000.000-00', {reverse: false});
		
	});
</script>
@endsection