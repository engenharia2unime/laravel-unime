@extends('layouts.layout') 
@section('content')
 			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Leituras</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
<!--             seta apenas na index geral e não nas internas -->
             @if(!isset($relogio))
             	<div class="row">
	            	<div class="col-lg-12">
	 					@include('back.leitura.filtrar')
	 				</div>
 				</div><br/>
             @endif
             
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
<!--                         	Se se já existe o relogio setado na url -->
                        	@if(isset($relogio))
                        		<a class="btn btn-primary" href="{{ action('LeituraController@create', $relogio) }}">Adicionar Leitura</a>
                        	@else
                        		<a class="btn btn-primary" href="{{ action('LeituraController@createWithClock') }}">Adicionar Leitura</a>
                        	@endif
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Data</th>
                                            @if(!isset($relogio))
                                            	<th>Relógio</th>
                                            @endif
                                            <th>Quantidade KW</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($leituras as $leitura)	
                                        <tr class="odd gradeX">
                                            <td>{{ Helper::formatDateToView($leitura->dataLeitura) }}</td>
                                            @if(!isset($relogio))
                                            	 <td>{{ $leitura->relogio->identificacao }}</td>
                                            @endif
                                            <td>{{ $leitura->quantidade }}</td>
                                            <td style="width:300px">
													<a class="btn btn-primary" href="">Editar</a>
													<a class="btn btn-danger" href="">Deletar</a>
											</td>
                                        </tr>
                                   @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@endsection