@extends('layouts.layout') 
@section('content')

	<h2>Cadastro de Leituras</h2>
	<hr/>
	
	{!! Form::open(array('url' => 'leitura')) !!}
		
		{!! Form::label('relogio', 'Relógio: ') !!}
		<div class="input-group">
			{!! Form::text('relogio', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
			<a class="btn btn-primary" type="" id="buscarRelogio">Buscar</a>
		</div><br/>
		
		<div class="form-group">
			{!! Form::label('quantidade', 'Quantidade KW: ') !!}
			{!! Form::text('quantidade', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		</div>
		
		<div class="form-group">
			{!! Form::label('dataLeitura', 'Data da Leitura: ') !!}
			{!! Form::date('dataLeitura', null, array('class' => 'form-control', 'style' => 'width:300px')) !!}
		</div>
		
		{!! Form::hidden('relogioId', null, array('class' => 'form-control', 'style' => 'width:300px', 'id' => 'relogioId')) !!}
		
		<div class="form-group">
			{!! Form::submit('Enviar', ['class' => 'btn btn-default']) !!}
			<a class="btn btn-danger" href="/leitura/index/">Cancelar</a>
		</div>
	{!! Form::close() !!}

	
<script language="javascript" type="text/javascript">

	$(document).ready(function(){
		$('.cpf_cnpj').mask('000.000.000-00', {reverse: false});
		$('.telefone').mask('(00) 0000-00000');
		$('.cep').mask('00000-000');
		$('#cliente').mask('000.000.000-00', {reverse: false});

		$("#buscarRelogio").click(function(){
			var relogio = $('#relogio').val();
			relogio ? buscarRelogio(relogio) : alert('Digite um relógio válido!');
		});

		function buscarRelogio(relogio){
			$.ajax({
				  url: "/relogio/buscar-relogio",
				  dataType: 'json',
				  data: { relogio: relogio}
				})
			  .done(function(data) {
				var obj = JSON.parse(data.relogio);
				
				if(obj != ''){						// verificar se é o melhor método de condição para caso haja cliente
					alert( "Relógio Encontrado!"); // melhorar para algo mais carismático
				    $("#relogio").val(obj[0].identificacao);
				    $("#relogioId").val(obj[0].id);
				    
				}else{
					alert("Cliente não encontrado! Verifique o CPF digitado!");
					}
			  });
		}
		
	});
</script>
@endsection