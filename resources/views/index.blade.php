@extends('app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron well">
				<h1 class="text-center">
					UNILaravel
				</h1>
				<p class="text-center">
					Esse é o sistema exigido pelo trabalho de Engenharia de Software 2, UNIME - 2015.<br> 
					O sistema simula um pequeno fluxo de processo da Coelba, mais especificamente entre o técnico e o cliente.
					Todo o código fonte encontra-se disponível no BitBucket e em breve no GitHub:
				</p>
				<p>
					<a class="btn btn-primary btn-large" href="https://bitbucket.org/engenharia2unime/laravel-unime/">BitBucket</a>
				</p>
			</div>
		</div>
	</div>
	<p class="text-center">
		Lorem ipsum dolor sit amet, <strong>consectetur adipiscing elit</strong>. Aliquam eget sapien sapien. Curabitur in metus urna. In hac habitasse platea dictumst. Phasellus eu sem sapien, sed vestibulum velit. Nam purus nibh, lacinia non faucibus et, pharetra in dolor. Sed iaculis posuere diam ut cursus. <em>Morbi commodo sodales nisi id sodales. Proin consectetur, nisi id commodo imperdiet, metus nunc consequat lectus, id bibendum diam velit et dui.</em> Proin massa magna, vulputate nec bibendum nec, posuere nec lacus. <small>Aliquam mi erat, aliquam vel luctus eu, pharetra quis elit. Nulla euismod ultrices massa, et feugiat ipsum consequat eu.</small>
	</p>
	<div class="row">
				<div class="col-md-4">
					<div class="thumbnail">
						<img alt="300x200" src="{{ asset('app/images/thumbnail1.jpg') }}" />
						<div class="caption">
							<h3>
								Thumbnail label
							</h3>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
							<p>
								<a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="thumbnail">
						<img alt="300x200" src="{{ asset('app/images/thumbnail3.jpg') }}" />
						<div class="caption">
							<h3>
								Thumbnail label
							</h3>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
							<p>
								<a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="thumbnail">
						<img alt="300x200" src="{{ asset('app/images/thumbnail4.jpg') }}" />
						<div class="caption">
							<h3>
								Thumbnail label
							</h3>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
							<p>
								<a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
							</p>
						</div>
					</div>
				</div>
			</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		$('.cpf').mask('000.000.000-00', {reverse: false});
		$('.telefone').mask('(00) 0000-00000');
		$('.cep').mask('00000-000');
		});
</script>
@endsection