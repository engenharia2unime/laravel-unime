@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Serviços ao Cliente</div>
				<h3 class="page-header">Consulta de clientes por CPF</h3>
				<div class="panel-body">
					{!! Form::open(array('action' => 'FrontClienteController@consultar')) !!}
		
					<div class="form-group">
						{!! Form::label('cpf', 'CPF: ') !!}
						{!! Form::text('cpf', null, array('class' => 'form-control cpf', 'style' => 'width:300px')) !!}
					
					</div>
					
					<div class="form-group">
						{!! Form::submit('Consultar', ['class' => 'btn btn-default']) !!}
						<button class="btn btn-danger">Cancelar</button>
					</div>
				{!! Form::close() !!}
				</div>
				
				<div class="panel-body">
					@if(isset($cliente))
						<h3 class="page-header">Cliente encontrado: </h3>
							<table class="table table-striped">
							<tr>
								<th>Nome do cliente: </th>
								<td>{{ $cliente->nome }}</td>
							</tr>
							<tr>
								<th>Email: </th>
								<td>{{ $cliente->email }}</td>
							</tr>
							<tr>
								<th>Telefone: </th>
								<td>{{ $cliente->telefone }}</td>
							</tr>
							</table>
				</div>
					@endif
					
					@if(isset($noResults))
						@if($noResults != '')
						<div class="panel-body">
							<h3 class="page-header">Nenhum cliente encontrado! </h3>
						</div>
						@endif
					@endif
				</div>
				
			</div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		$('.cpf').mask('000.000.000-00', {reverse: false});
		$('.telefone').mask('(00) 0000-00000');
		$('.cep').mask('00000-000');
		});
</script>
@endsection