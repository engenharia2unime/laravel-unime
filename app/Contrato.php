<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model {

	protected $table = 'contrato';
	protected $fillable = ['clienteId', 'dataCriacao', 'numero', 'enderecoId', 'precoKW'];
	
	/*
	 * @param $attributes = atributos a serem salvos na model especifica
	 * @param $data = todos os dados do input, caso seja preciso manipular outra entidade
	 * @return 
	 */
// 	public static function create(array $attributes, array $data){
		
// 		$endereco = array();
// 		$endereco['logradouro'] = $data['logradouro'];
// 		$endereco['cidade'] = $data['cidade'];
// 		$endereco['bairro'] = $data['bairro'];
// 		$endereco['cep'] = $data['cep'];
// 		$endereco['estado'] = $data['estado'];
// 		$enderecoId = Endereco::create($endereco);
		
		
// 		$attributes['enderecoId'] = $enderecoId->id;
// 		$contrato = new static($attributes);
// 		$contrato->save();
		
// 		return $contrato;
// 	}
	
	public function contas()
	{
		return $this->belongsTo('App\Conta', 'id', 'contratoId');
	}
	
	public function relogios()
	{
		return $this->belongsTo('App\Relogio', 'id', 'contratoId');
	}
	
	public function cliente()
	{
		return $this->hasOne('App\Cliente', 'id', 'clienteId');
	}
	
	public function endereco()
	{
		return $this->hasOne('App\Endereco', 'id', 'enderecoId');
	}

}
