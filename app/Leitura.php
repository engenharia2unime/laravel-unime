<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Leitura extends Model {

	protected $table = 'leitura';
	protected $fillable = ['relogioId', 'tecnicoId', 'dataLeitura', 'quantidade'];
	
	public static function create(array $attributes){
		$leitura = new static($attributes);
		$leitura->save();
		
		$conta = array();
		$conta['leituraId'] = $leitura->id;
		$conta['valorTotal'] = $leitura->relogio->contrato->precoKW * $leitura->quantidade;
		$conta['contratoId'] = $leitura->relogio->contrato->id;
		$conta['numero'] = date('Ymdh').rand().$leitura->relogio->contrato->numero;
		$conta['dataVencimento'] = date('Y-m-d', strtotime("+30 days") );
		
		Conta::create($conta);
		
		return $leitura;
	}
	
	public function relogio()
	{
		return $this->hasOne('App\Relogio', 'id', 'relogioId');
	}
	
	public function tecnico()
	{
		return $this->hasOne('App\Tecnico', 'id', 'tecnicoId');
	}
	
	public function contas()
	{
		return $this->belongsTo('App\Conta', 'id', 'leituraId');
	}

}
