<?php

use Illuminate\Support\Facades\Input;
/**
 * Front
 */
Route::get('/', 'IndexController@frontSite');
Route::get('/servicos-cliente', 'FrontClienteController@index');
Route::post('/servicos-cliente/consultar', 'FrontClienteController@consultar');

/**
 * AUTH
 */
Route::get('/login', 'Auth\AuthController@getLogin');
Route::get('/logout', 'Auth\AuthController@getLogout');
Route::get('/registrar', 'Auth\AuthController@getRegister');

/*
 * IndexController
 */
Route::get('dashboard/index', 'IndexController@indexAdmin');


/*
 * Cliente Routes
 */
Route::group(['middleware' => 'auth'], function()
{
	Route::post('cliente', 'ClienteController@store');
	Route::post('cliente/buscar', 'ClienteController@search');
	Route::get('/cliente/buscar/{cliente}', 'ClienteController@renderSearch');
	Route::get('/cliente/index', 'ClienteController@index');
	Route::get('/cliente/buscar-cliente', 'ClienteController@getClienteByCpf');
	Route::get('/cliente/novo', 'ClienteController@create');
	Route::get('cliente/editar/{id}', ['as' => 'clienteEditar', 'uses'=> 'ClienteController@edit']);
	Route::put('cliente/{id}', ['as' => 'clienteUp', 'uses'=> 'ClienteController@update']);
	Route::get('cliente/{id}', 'ClienteController@destroy');
});


/*
 * Contrato Routes
 */
Route::group(['middleware' => 'auth'], function()
{
	Route::post('contrato', 'ContratoController@store');
	Route::post('contrato/buscar', 'ContratoController@search');
	Route::get('/contrato/buscar/{cliente}', 'ContratoController@renderSearch');
	Route::get('/contrato/index', 'ContratoController@index');
	Route::get('/contrato/novo', 'ContratoController@create');
	Route::get('contrato/editar/{id}', ['as' => 'contratoEditar', 'uses'=> 'ContratoController@edit']);
	Route::put('contrato/{id}', ['as' => 'contratoUp', 'uses'=> 'ContratoController@update']);
});

/*
 * Relógios Routes
 */
Route::group(['middleware' => 'auth'], function()
{
	Route::post('relogio', 'RelogioController@store');
	Route::get('/relogio/contrato/{contratoId}', 'RelogioController@index');
	Route::get('/relogio/novo/{contratoId}', 'RelogioController@create');
	Route::get('/relogio/buscar-relogio', 'RelogioController@getRelogioByIdentificao');
	
});


/*
 * Leituras Routes
 */
Route::group(['middleware' => 'auth'], function()
{
	Route::post('leitura', 'LeituraController@store');
	Route::post('leitura/buscar', 'LeituraController@search');
	Route::get('/leitura/buscar/{relogio}', 'LeituraController@renderSearch');
	Route::get('/leitura/index', 'LeituraController@listar');
	Route::get('/leitura/relogio/{relogioId}', 'LeituraController@index');
	Route::get('/leitura/novo/{relogioId}', 'LeituraController@create');
	Route::get('/leitura/novo', 'LeituraController@createWithClock');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);