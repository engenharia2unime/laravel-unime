<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Cliente;
use App\Contrato;
use App\Leitura;

class IndexController extends Controller {

	public function index()
	{
		return Redirect::route('login');
	}
	
	public function frontSite()
	{
		return view('index');
	}
	
	public function indexAdmin()
	{
		
		$clientes = Cliente::all()->count();
		$contratos = Contrato::all()->count();
		$leituras = Leitura::all()->count();
		
		return view('back.index')->with(['clientes' => $clientes, 
										'contratos' => $contratos,
										'leituras' => $leituras
										]);
	}

}
