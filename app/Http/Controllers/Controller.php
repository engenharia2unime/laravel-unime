<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	protected $model;
	
	protected function storeActionPadrao($data = array(), $options = array())
	{
		$save = array();
		$fillable = $this->model->getFillable();
		
		foreach ($fillable as $fill){
			if(isset($data[$fill])){
				$save[$fill] = $data[$fill];
			}
		}
		
		$this->model->create($save);
		
		return redirect($options['retorno']);
	}
	
}
