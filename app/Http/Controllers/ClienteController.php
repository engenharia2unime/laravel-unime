<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Request;
use App\Cliente;
use App\ClienteEndereco;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\Contrato;
use Illuminate\Support\Facades\Redirect;

class ClienteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$clientes = Cliente::all();
		return view('back.clientes.index')->with(['clientes' => $clientes]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('back.clientes.novo');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Request::all();
		
		
		if ($input){
			$novoCliente = array();
			$novoCliente['nome'] = $input['nome'];
			$novoCliente['cpf_cnpj'] = $input['cpf_cnpj'];
			$novoCliente['email'] = $input['email'];
			$novoCliente['telefone'] = $input['telefone'];
			Cliente::create($novoCliente);
			
			return redirect('/cliente/index');
		}
	}

	
	public function search()
	{
		$input = Request::all();
		$cliente = $input['cliente'];
	
		return Redirect::action('ClienteController@renderSearch', array('cliente' => $cliente));
	}
	
	public function renderSearch($cliente)
	{
		$results = Cliente::where('nome', 'LIKE', "%$cliente%")->get();
			
		return view('back.clientes.index')->with(['clientes' => $results, 'busca' => $cliente]);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// DTO Pattern
// 		$user = Cliente::where('cliente.id', '=', $id)
// 			->join('endereco', 'endereco.clienteId', '=', 'cliente.id')
// 			->first();
		
		$user = Cliente::find($id);
		return view('back.clientes.editar')->with(['cliente' => $user]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Request::all();
		$cliente = Cliente::find($id);
		
		if ($input){
			$novoCliente = array();
			$novoCliente['nome'] = $input['nome'];
			$novoCliente['cpf_cnpj'] = $input['cpf_cnpj'];
			$novoCliente['email'] = $input['email'];
			$novoCliente['telefone'] = $input['telefone'];
			$cliente->update($novoCliente);
				
			return redirect('/cliente/index');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Cliente::find($id)->delete();
		return redirect('/cliente/index')->with('message', 'Registro apagado com sucesso!');
	}

	
	/**
	 * Retorna o resultado de clientes de acordo
	 * com o CPF pesquisado na view
	 *
	 * @return Response
	 */
	public function getClienteByCpf()
	{
		if(Input::get('cliente')){
			
			$cpf = Input::get('cliente');
			$clientes = Cliente::where('cpf_cnpj', 'LIKE', "%$cpf%")->get()->toArray();
			$retorno = json_encode($clientes);
			
			$response = array(
					'status' => 'success',
					'data' => 'AJAX!',
					'cliente' => $retorno);
			
			return response()->json($response);
		}
	}
}
