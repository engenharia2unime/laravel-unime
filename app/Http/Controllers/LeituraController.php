<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Request;
use App\Leitura;
use App\Tecnico;
use Illuminate\Support\Facades\Redirect;

class LeituraController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($relogioId)
	{
		$leituras = Leitura::where('relogioId', '=', $relogioId)->get();
		return view('back.leitura.index')->with(['leituras' => $leituras, 'relogio' => $relogioId]);
	}

	public function listar()
	{
		$leituras = Leitura::with('relogio')->orderBy('leitura.dataLeitura', 'desc')->get();
		return view('back.leitura.index')->with(['leituras' => $leituras]);
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($relogioId)
	{
		return view('back.leitura.novo')->with(['relogio' => $relogioId]);
	}
	
	public function createWithClock()
	{
		return view('back.leitura.novo-with-clock');
	}
	
	public function search()
	{
		$input = Request::all();
		$relogio = $input['relogio'];
	
		return Redirect::action('LeituraController@renderSearch', array('relogio' => $relogio));
	}
	
	public function renderSearch($relogio)
	{
		$results = Leitura::whereHas('relogio', function($query) use($relogio){
			$query->where('identificacao', '=', $relogio);
		})->get();
			
		return view('back.leitura.index')->with(['leituras' => $results, 'busca' => $relogio]);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Request::all();
		
		$user =\Auth::user();
		$tecnico = Tecnico::where('userId', '=', $user->id)->get()->first();
		$input['tecnicoId'] = $tecnico->id;
		
		$this->model = new Leitura();
		$options = array('retorno' => '/leitura/relogio/'.$input['relogioId']);
		
		return $this->storeActionPadrao($input, $options);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
