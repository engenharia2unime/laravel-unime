<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use App\Cliente;

class FrontClienteController extends Controller {

	public function index()
	{
		return view('servicos-cliente');
	}
	
	public function consultar()
	{
		$input = Request::all();
		$cpf = $input['cpf'];
		$cliente = Cliente::where('cpf_cnpj', '=', $cpf)->first();
		$noResults = '';
		
		if(!$cliente){
			$noResults = 'Desculpe, mas não há nenhum cliente com esse CPF cadastro!';
		}
		
		return view('servicos-cliente')->with(['cliente' => $cliente, 'noResults' => $noResults]);
	}
}
