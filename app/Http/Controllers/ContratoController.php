<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use Illuminate\Http\Response;
use App\Contrato;
use App\Endereco;
use Illuminate\Support\Facades\Redirect;
use App\Cliente;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ContratoController extends Controller {

	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$contratos = Contrato::all();
		return view('back.contrato.index')->with(['contratos' => $contratos]);
	}

	public function search()
	{
		$input = Request::all();
		$cliente = $input['cliente'];
		
		return Redirect::action('ContratoController@renderSearch', array('cliente' => $cliente));
	}
	
	public function renderSearch($cliente)
	{
		$results = Contrato::whereHas('cliente', function($query) use($cliente){
			$query->where('nome', 'LIKE', "%$cliente%");
		})->get();
			
		return view('back.contrato.index')->with(['contratos' => $results, 'busca' => $cliente]);
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('back.contrato.novo');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Request::all();
		
		if ($input){
			$endereco = array();
			$endereco['logradouro'] = $input['logradouro'];
			$endereco['cidade'] = $input['cidade'];
			$endereco['bairro'] = $input['bairro'];
			$endereco['cep'] = $input['cep'];
			$endereco['estado'] = $input['estado'];
			$enderecoId = Endereco::create($endereco);
			
			$contrato = array();
			$contrato['enderecoId'] = $enderecoId->id;
			$contrato['clienteId'] = $input['clienteId'];
			$contrato['numero'] = $input['numero'];
			Contrato::create($contrato);
			
			return redirect('/cliente/index');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// DTO Pattern
		// Apenas funciona se existir um endereço para o contrato, caso contrário todo o objeto será null
		$contrato = Contrato::where('contrato.id', '=', $id)
			->join('endereco', 'contrato.enderecoId', '=', 'endereco.id')
			->first();
		
		$contrato->id = $id; // refatorar o objeto para ter o id do contrato e não do endereço
		
		return view('back.contrato.editar')->with(['contrato' => $contrato]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Request::all();
		$contrato = Contrato::find($id);
		$enderecoBusca = Endereco::where('id', '=', $contrato->enderecoId)->first();
		$endereco = Endereco::find($enderecoBusca->id);
		
		if ($input){
			$novoEnd = array();
			$novoEnd['logradouro'] = $input['logradouro'];
			$novoEnd['cidade'] = $input['cidade'];
			$novoEnd['bairro'] = $input['bairro'];
			$novoEnd['cep'] = $input['cep'];
			$novoEnd['estado'] = $input['estado'];
			$endereco->update($novoEnd);
			
			$novoContrato = array();
			$novoContrato['enderecoId'] = $endereco->id;
			$novoContrato['numero'] = $input['numero'];
			$contrato->update($novoContrato);
				
			return redirect('/contrato/index');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
}
