<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Request;
use App\Relogio;

class RelogioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($contratoId)
	{
		$relogios = Relogio::where('contratoId', '=', $contratoId)->get();
		return view('back.relogio.index')->with(['relogios' => $relogios, 'contrato' => $contratoId]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($contratoId)
	{
		return view('back.relogio.novo')->with(['contrato' => $contratoId]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Request::all();
		$this->model = new Relogio();
		$options = array('retorno' => '/relogio/contrato/'.$input['contratoId']);
		
		return $this->storeActionPadrao($input, $options);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getRelogioByIdentificao()
	{
		if(Input::get('relogio')){
			$identificacao = Input::get('relogio');
			$relogios = Relogio::where('identificacao', 'LIKE', "%$identificacao%")->get()->toArray();
			$retorno = json_encode($relogios);
				
			$response = array(
					'status' => 'success',
					'data' => 'AJAX!',
					'relogio' => $retorno);
				
			return response()->json($response);
		}
	}

}
