<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model {

	protected $table = 'cliente';
	protected $fillable = ['nome', 'cpf_cnpj', 'email', 'telefone'];
	
	public function clienteEnderecos()
	{
		return $this->belongsTo('App\ClienteEndereco', 'id', 'clienteId');
	}
	
	public function contratos()
	{
		return $this->belongsTo('App\Contrato', 'id', 'clienteId');
	}

}
