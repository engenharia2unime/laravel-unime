<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Relogio extends Model {

	protected $table = 'relogio';
	protected $fillable = ['contratoId', 'identificacao'];
	
	public function contrato()
	{
		return $this->hasOne('App\Contrato', 'id', 'contratoId');
	}
	
	public function leituras()
	{
		return $this->belongsTo('App\Leitura', 'id', 'relogioId');
	}

}
