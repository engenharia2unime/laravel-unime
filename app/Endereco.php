<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model {

	protected $table = 'endereco';
	protected $fillable = ['logradouro', 'cidade', 'bairro', 'cep', 'estado'];
	
	
	public function contratos()
	{
		return $this->belongsTo('App\Contrato', 'id', 'enderecoId');
	}
	

}
