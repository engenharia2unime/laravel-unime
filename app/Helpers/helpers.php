<?php

/**
 * @author Felix Costa <fx3costa@gmail.com>
 * 
 */
class Helper {
 
	/**
	 * Retorna a data em um formato legível para a view, especialmente para as tabelas das views
	 * @param string $date
	 * @return date('d/m/Y', strtotime());
	 */
    public static function formatDateToView($date){
    	return date('d/m/Y', strtotime($date));
    }
 
}