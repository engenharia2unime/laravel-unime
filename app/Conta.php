<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Conta extends Model {

	protected $table = 'conta';
	protected $fillable = ['contratoId', 'leituraId' ,'dataEmissao', 'dataVencimento', 'valorTotal'];
	
	public function contratos()
	{
		return $this->hasOne('App\Contrato', 'id', 'contratoId');
	}
	
	public function leituras()
	{
		return $this->hasOne('App\Leitura', 'id', 'leituraId');
	}

}
