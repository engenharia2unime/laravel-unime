<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tecnico extends Model {

	protected $table = 'tecnico';
	protected $fillable = ['nome', 'regiaoAtuacao', 'userId'];
	
	public function leituras()
	{
		return $this->belongsTo('App\Leitura', 'id', 'tecnicoId');
	}
	
	public function user()
	{
		return $this->hasOne('App\User', 'id', 'userId');
	}

}
