<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tecnicotable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tecnico', function(Blueprint $table)
		{
			$table->increments('id');
// 			$table->integer('userId')->unsigned();
// 			$table->foreign('userId')->references('id')->on('users');
			$table->string('nome');
			$table->string('regiaoAtuacao');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tecnico');
	}

}
