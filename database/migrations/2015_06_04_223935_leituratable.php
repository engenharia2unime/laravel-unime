<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Leituratable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leitura', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tecnicoId')->unsigned();
			$table->integer('relogioId')->unsigned();
			$table->foreign('tecnicoId')->references('id')->on('tecnico');
			$table->foreign('relogioId')->references('id')->on('relogio');
			$table->dateTime('dataLeitura');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leitura');
	}

}
