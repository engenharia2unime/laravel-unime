<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clientetable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cliente', function(Blueprint $table)
		{
			$table->increments('id');
// 			$table->integer('userId')->unsigned();
// 			$table->foreign('userId')->references('id')->on('users');
			$table->string('nome');
			$table->string('cpf_cnpj');
			$table->string('email');
			$table->string('telefone');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cliente');
	}

}
