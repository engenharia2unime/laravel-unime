<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Relogiotable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('relogio', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('contratoId')->unsigned();
			$table->foreign('contratoId')->references('id')->on('contrato');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('relogio');
	}

}
