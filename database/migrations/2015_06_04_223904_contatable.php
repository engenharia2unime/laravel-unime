<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contatable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conta', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('contratoId')->unsigned();
			$table->foreign('contratoId')->references('id')->on('contrato');
			$table->dateTime('dataEmissao');
			$table->dateTime('dataVencimento');
			$table->string('valorTotal');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conta');
	}

}
