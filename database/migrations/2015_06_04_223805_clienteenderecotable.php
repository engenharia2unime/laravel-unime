<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clienteenderecotable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cliente_endereco', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('clienteId')->unsigned();
			$table->foreign('clienteId')->references('id')->on('cliente');
			$table->string('logradouro');
			$table->string('cidade');
			$table->string('bairro');
			$table->string('cep');
			$table->string('estado');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cliente_endereco');
	}

}
